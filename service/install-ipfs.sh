#!/bin/bash
#
# Description:
# Install Go IPFS daemon as a systemd service
#
# Authors:
# Jose G. Faisca <jose.faisca@gmail.com>
#

# check permission
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

# dependencies
apt-get install -y curl

# version (argument)
IPFS_VERSION=$1

# default version
IPFS_VERSION=${IPFS_VERSION:-"v0.4.19"}

# variables
FILE="go-ipfs_${IPFS_VERSION}_linux-amd64.tar.gz"
URL="https://dist.ipfs.io/go-ipfs/${IPFS_VERSION}"
BIN_PATH="/usr/local/bin"
USER_NAME="root"
GROUP="root"
ARGUMENTS="--enable-namesys-pubsub"
HOME_DIR=$(getent passwd "$USER_NAME" | cut -d: -f6)
IPFS_PATH="$HOME_DIR/.ipfs"

# install
curl -O $URL/$FILE
tar xvzf $FILE
mv go-ipfs/ipfs $BIN_PATH/ipfs
rm -rf go-ipfs
rm -f $FILE

# print IPFS version
ipfs version

# initialize IPFS
if [ ! -f $IPFS_PATH/config ]; then
   export IPFS_PATH="$HOME_DIR/.ipfs"
   exec "$@"
   su $USER_NAME -c "ipfs init"
fi

# test if ipfs.service is running
systemctl is-active --quiet ipfs.service && systemctl stop ipfs.service

# create ipfs.service
cat > /lib/systemd/system/ipfs.service << EOF
[Unit]
Description=ipfs daemon

[Service]
ExecStart=$BIN_PATH/ipfs daemon $ARGUMENTS
Restart=always
User=$USER_NAME
Group=$GROUP

[Install]
WantedBy=multi-user.target
EOF

# reload systemctl daemon
systemctl daemon-reload

# enable ipfs.service
systemctl enable ipfs.service

# start ipfs.service
systemctl start ipfs.service

sleep 3

# see the ipfs.service status
systemctl status ipfs.service

exit 0
