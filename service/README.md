# Set up IPFS daemon 
Run ipfs daemon as a systemd service (without Docker)

#### Run installation script
```
$ sudo ./install-ipfs.sh [version]
```
The script installs ipfs binaries and setup and starts 'ipfs.service'. 
As a default, the service will run as root, modify USER_NAME and GROUP variables to run as another Linux user/group.

#### See the ipfs.service status
```
$ systemctl status ipfs.service
```

#### Stop the ipfs.service status
```
$ systemctl stop ipfs.service
```

#### Start ipfs.service
```
$ systemctl start ipfs.service
```

#### See the last 20 lines of logs from ipfs.service
```
$ journalctl -u ipfs -n20
```

#### The output is below, make a note of the address that the gateway is listening on.
```
-- Logs begin at Sun 2019-02-17 15:57:17 UTC, end at Sat 2019-04-13 11:23:03 UTC. --
Apr 13 10:44:55 host1-lxc4 systemd[1]: Started ipfs daemon.
Apr 13 10:44:56 host1-lxc4 ipfs[15045]: Initializing daemon...
Apr 13 10:44:56 host1-lxc4 ipfs[15045]: go-ipfs version: 0.4.19-
Apr 13 10:44:56 host1-lxc4 ipfs[15045]: Repo version: 7
Apr 13 10:44:56 host1-lxc4 ipfs[15045]: System version: amd64/linux
Apr 13 10:44:56 host1-lxc4 ipfs[15045]: Golang version: go1.11.5
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Swarm listening on /ip4/127.0.0.1/tcp/4001
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Swarm listening on /ip4/192.168.15.54/tcp/4001
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Swarm listening on /ip6/::1/tcp/4001
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Swarm listening on /p2p-circuit
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Swarm announcing /ip4/127.0.0.1/tcp/4001
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Swarm announcing /ip4/192.168.15.54/tcp/4001
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Swarm announcing /ip6/::1/tcp/4001
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: API server listening on /ip4/127.0.0.1/tcp/5001
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: WebUI: http://127.0.0.1:5001/webui
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Gateway (readonly) server listening on /ip4/0.0.0.0/tcp/8080
Apr 13 10:44:58 host1-lxc4 ipfs[15045]: Daemon is ready
```
